#include "../01_context/context.h"
#include "../02_pipeline/pipeline.h"
#include "../03_depth_buffer/random_scene.h"
#include "viewing.h"

// parameters for procedural scene
int   nr_triangles = 1000;
float edge_length_scale = 0.2f;
std::vector<glm::vec3> vertex_data;

// store ids of render objects globally
GLuint vertex_shader_id = -1, fragment_shader_id = -1, shader_program_id = -1;
GLuint vertex_buffer_id = -1, vertex_array_id = -1;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
		return;
	switch (key) {
	case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE); break;
	}
}

// prepare context and render objects
void init(GLFWwindow* window, int w, int h)
{
	// define color to clear the color buffer in the glClear() call in draw() function
	glClearColor(1.0f, 0.5f, 0.7f, 1.0f);

	// configure and enable depth test
	glClearDepth(1.0f);       // set initial value with which to clear depth buffer to 1.0 corresponding to z_far
	glEnable(GL_DEPTH_TEST);  // enable depth test
	glDepthFunc(GL_LESS);     // let fragments pass test that have smaller depth value than depth buffer

	// create shader program and query uniform location
	shader_program_id = create_program(
		"../02_pipeline/simple_shader.glvs", GL_VERTEX_SHADER, &vertex_shader_id,
		"../02_pipeline/simple_shader.glfs", GL_FRAGMENT_SHADER, &fragment_shader_id, 0);

	// call to initialize viewing
	init_viewing(window, w, h, shader_program_id);

	glfwSetKeyCallback(window, key_callback);

	// create vertex buffer of random scene and vertex array
	create_random_triangle_scene(vertex_data, nr_triangles, edge_length_scale);
	vertex_buffer_id = create_buffer(glm::value_ptr(vertex_data[0]), GLsizei(3 * vertex_data.size()));
	vertex_array_id = create_vertex_array_with_position_and_color(shader_program_id, vertex_buffer_id, 3, 3);
}

// delete render objects
void cleanup()
{
	if (shader_program_id != -1)
		glDeleteProgram(shader_program_id);
	if (vertex_shader_id != -1)
		glDeleteShader(vertex_shader_id);
	if (fragment_shader_id != -1)
		glDeleteShader(fragment_shader_id);
	if (vertex_buffer_id != -1)
		glDeleteBuffers(1, &vertex_buffer_id);
	if (vertex_array_id != -1)
		glDeleteVertexArrays(1, &vertex_array_id);
}

// draw a triangle with interpolated colors
void display()
{
	// clear the buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// validity check for shader and vertex array
	if (shader_program_id == -1)
		return;
	if (vertex_array_id == -1)
		return;


	// enable shader program
	glUseProgram(shader_program_id);

	// set model view projection matrix
	set_viewing_parameters(shader_program_id);

	// activate vertex array object to specify where vertex data comes from
	glBindVertexArray(vertex_array_id);

	// finally draw the vertex data
	glDrawArrays(
		GL_TRIANGLES, // drawing mode specified primitive type and whether specification is via strips
		0,            // index of first vertex in arrays
		GLsizei(3*vertex_data.size()/2)  // count of to be drawn vertices (not triangle count!!)
	);

	// deactivate vertex array and shader program
	glBindVertexArray(0);
	glUseProgram(0);
}

// main function initializes glfw, creates window, initializes glew and app before main loop is started
int main(int argc, char *argv[])
{
	std::cout << "HELP" << std::endl;
	std::cout << "-----------------------------------" << std::endl;
	std::cout << "<Left Mouse Button>  ... rotate view" << std::endl;
	std::cout << "<Right Mouse Button> ... translate view" << std::endl;
	std::cout << "<Mouse Wheel>        ... zoom view" << std::endl;
	std::cout << "<Escape>     ... exit sample" << std::endl;
	std::cout << "-----------------------------------" << std::endl;
	return context_setup_and_main_loop(640, 480, "cg1_viewing_sample");
}
