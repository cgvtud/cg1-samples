#pragma once

#include <map>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtc/type_ptr.hpp> // necessary for glm::value_ptr() function

// mouse state
bool button_state[8] = { false, false, false, false, false, false, false, false };
double last_x, last_y;

// viewing parameters used to define lookat transformation
glm::vec3 focus(0,0,0);
glm::vec3 eye(0,1,-5);
glm::vec3 view_up(0,1,0);

glm::mat4 projection_matrix;

struct shader_matrix_id_type
{
	GLint  model_view_matrix_id;
	GLint  view_matrix_id;
	GLint  normal_matrix_id;
	GLint  model_view_projection_matrix_id;
	GLint  projection_matrix_id;
};

shader_matrix_id_type& ref_shader_matrix_id_type(GLint prog_id)
{
	static std::map<GLint, shader_matrix_id_type> ids;
	return ids[prog_id];
}

enum MouseMode {
	MM_NONE,
	MM_TRANSLATE,
	MM_ROTATE
};

MouseMode mouse_mode = MM_NONE;

// prepare context and render objects
void resize(GLFWwindow* window, int w, int h)
{
	// define viewport from window extent
	glViewport(0, 0, w, h);

	float aspect = (float)w / h;
	GLdouble z_near = 0.02f;
	projection_matrix = glm::frustum(-0.5 * aspect * z_near, 0.5 * aspect * z_near, -0.5 * z_near, 0.5 * z_near, z_near, 100.0);
}

void cursor_pos_callback(GLFWwindow* window, double x, double y)
{
	int w, h;
	glfwGetWindowSize(window, &w, &h);
	float dx = float(x - last_x)/w;
	float dy = float(last_y - y)/h; // negate y coordinate as OpenGL y goes up and window y down
	last_x = x;
	last_y = y;
	float distance = length(eye - focus);
	glm::vec3 z_dir = (1.0f/distance)*(eye - focus);
	glm::vec3 x_dir = normalize(cross(view_up, z_dir));
	glm::vec3 y_dir = cross(z_dir, x_dir);
	glm::vec3 drag_vector = distance * (dx * x_dir + dy * y_dir);

	switch (mouse_mode) {
	case MM_TRANSLATE :
		focus -= drag_vector;
		eye   -= drag_vector;
		break;
	case MM_ROTATE :
	{
		glm::vec3 axis = normalize(dx*y_dir - dy * x_dir);
		float angle = 3*sqrt(dx*dx + dy * dy);
		glm::mat3 R(glm::rotate(glm::identity<glm::mat4>(), -angle, axis));
		eye     = R * (eye - focus) + focus;
		view_up = normalize(R * view_up);
	}
		break;
	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int modifiers)
{
	if (button_state[button] = action == GLFW_PRESS) {
		switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:  mouse_mode = MM_ROTATE; break;
		case GLFW_MOUSE_BUTTON_RIGHT: mouse_mode = MM_TRANSLATE; break;
		}
	}
	else {
		if (mouse_mode == MM_ROTATE && button == GLFW_MOUSE_BUTTON_LEFT)
			mouse_mode = MM_NONE;
		if (mouse_mode == MM_TRANSLATE && button == GLFW_MOUSE_BUTTON_RIGHT)
			mouse_mode = MM_NONE;
	}
}

void scroll_callback(GLFWwindow* window, double dx, double dy)
{
	float distance = length(eye - focus);
	glm::vec3 z_dir = (1.0f / distance)*(eye - focus);
	distance *= float(pow(0.8f, dy));
	eye = focus + distance * z_dir;
}

// extract and store shader ids for given shader program
void store_matrix_shader_ids(GLint prog_id)
{
	if (prog_id == -1)
		return;
	shader_matrix_id_type& ids = ref_shader_matrix_id_type(prog_id);
	ids.model_view_projection_matrix_id = glGetUniformLocation(prog_id, "modelViewProjectionMatrix");
	ids.projection_matrix_id = glGetUniformLocation(prog_id, "projectionMatrix");
	ids.view_matrix_id = glGetUniformLocation(prog_id, "viewMatrix");
	ids.model_view_matrix_id = glGetUniformLocation(prog_id, "modelViewMatrix");
	ids.normal_matrix_id = glGetUniformLocation(prog_id, "normalMatrix");
}

// set callbacks and initialize viewing
void init_viewing(GLFWwindow* window, int w, int h, GLint prog_1_id, GLint prog_2_id = -1, GLint prog_3_id = -1)
{
	// add callbacks
	glfwSetWindowSizeCallback(window, resize);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, cursor_pos_callback);
	glfwSetScrollCallback(window, scroll_callback);
	resize(window, w, h);

	store_matrix_shader_ids(prog_1_id);
	store_matrix_shader_ids(prog_2_id);
	store_matrix_shader_ids(prog_3_id);
}

glm::mat4 get_view_matrix()
{
	return glm::lookAt(eye, focus, view_up);
}


glm::mat4 get_model_view_matrix(glm::mat4* model_matrix_ptr = 0)
{
	glm::mat4 model_matrix = model_matrix_ptr ? *model_matrix_ptr : glm::identity<glm::mat4>();
	return get_view_matrix() * model_matrix;
}

void set_viewing_parameters(GLint prog_id, glm::mat4* model_matrix_ptr = 0)
{
	glm::mat4 view_matrix = get_view_matrix();

	glm::mat4 model_view_matrix = get_model_view_matrix(model_matrix_ptr);

	glm::mat3 normal_matrix(model_view_matrix);
	normal_matrix = transpose(inverse(normal_matrix));
	
	glm::mat4 model_view_projection_matrix = projection_matrix*model_view_matrix;

	shader_matrix_id_type& ids = ref_shader_matrix_id_type(prog_id);
	if (ids.model_view_projection_matrix_id != -1)
		glUniformMatrix4fv(ids.model_view_projection_matrix_id, 1, GL_FALSE,
			glm::value_ptr(model_view_projection_matrix));
	if (ids.projection_matrix_id != -1)
		glUniformMatrix4fv(ids.projection_matrix_id, 1, GL_FALSE,
			glm::value_ptr(projection_matrix));
	if (ids.view_matrix_id != -1)
		glUniformMatrix4fv(ids.view_matrix_id, 1, GL_FALSE,
			glm::value_ptr(view_matrix));
	if (ids.model_view_matrix_id != -1)
		glUniformMatrix4fv(ids.model_view_matrix_id, 1, GL_FALSE,
			glm::value_ptr(model_view_matrix));
	if (ids.normal_matrix_id != -1)
		glUniformMatrix3fv(ids.normal_matrix_id, 1, GL_FALSE,
			glm::value_ptr(normal_matrix));
}
