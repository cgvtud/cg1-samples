# cg1-samples

A collection of samples for the Computer Graphics I lecture.

# usage

To support synchronous OpenGL debugging one needs to use shared libraries, which is enforced in the main
CMakeLists.txt file. Therefore, simply generate your solution or workspace with cmake. The 3D models used
in examples 05 - 09 can be downloaded from [models.zip (12.9MB)](https://tu-dresden.de/ing/informatik/smt/cgv/ressourcen/dateien/lehre/ws19-20/cg1/models.zip).
Use the password for the lecture without the semester postfix.