#include "../01_context/context.h"
#include "pipeline.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp> // necessary for glm::value_ptr() function
#include <iostream>
#include <string>
#include <vector>

// store ids of render objects globally
GLuint vertex_shader_id = -1, fragment_shader_id = -1, shader_program_id = -1;
GLuint vertex_buffer_id = -1, vertex_array_id = -1;
GLint  model_view_projection_matrix_id = -1;

// prepare context and render objects
void init(GLFWwindow* window, int w, int h)
{
	// define color to clear the color buffer in the glClear() call in draw() function
	glClearColor(1.0f, 0.5f, 0.7f, 1.0f);

	// define viewport from window extent
	glViewport(0, 0, w, h);

	// create shader program and query uniform location
	shader_program_id = create_program("simple_shader.glvs", GL_VERTEX_SHADER, &vertex_shader_id, 
								       "simple_shader.glfs", GL_FRAGMENT_SHADER, &fragment_shader_id, 0);
	model_view_projection_matrix_id = glGetUniformLocation(shader_program_id, "modelViewProjectionMatrix");

	// declare vertex data with one vertex per line containing 4d position and 4d color values
	GLfloat vertex_data[] = {
		-0.5f, -0.5f, 0.0f, 1.0f,    1.0f, 0.0f, 0.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f, 1.0f,
		 0.0f,  0.5f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f, 1.0f
	};
	vertex_buffer_id = create_buffer(vertex_data, 24);
	vertex_array_id = create_vertex_array_with_position_and_color(shader_program_id, vertex_buffer_id, 4, 4);
}

// delete render objects
void cleanup()
{
	if (shader_program_id != -1)
		glDeleteProgram(shader_program_id);
	if (vertex_shader_id != -1)
		glDeleteShader(vertex_shader_id);
	if (fragment_shader_id != -1)
		glDeleteShader(fragment_shader_id);
	if (vertex_buffer_id != -1)
		glDeleteBuffers(1, &vertex_buffer_id);
	if (vertex_array_id != -1)
		glDeleteVertexArrays(1, &vertex_array_id);
}

// draw a triangle with interpolated colors
void display()
{
	// clear the buffers
	glClear(GL_COLOR_BUFFER_BIT);

	// validity check for shader and vertex array
	if (shader_program_id == -1)
		return;
	if (vertex_array_id == -1)
		return;

	// construct identity transformation matrix with glm
	glm::mat4 modelviewprojection(1.0f);

	// enable shader program
	glUseProgram(shader_program_id);

	// set tranformation matrix
	glUniformMatrix4fv(
		model_view_projection_matrix_id,             // uniform location of modelviewprojection matrix is determined in init() function
		1,                                  // a single matrix
		GL_FALSE,                           // do not transpose, which would be necessary if matrix is stored in row major format
		glm::value_ptr(modelviewprojection) // access to matrix data 
	);

	// activate vertex array object to specify where vertex data comes from
	glBindVertexArray(vertex_array_id);

	// finally draw the vertex data
	glDrawArrays(
		GL_TRIANGLES, // drawing mode specified primitive type and whether specification is via strips
		0,            // index of first vertex in arrays
		3             // count of to be drawn vertices (not triangle count!!)
	);

	// deactivate vertex array and shader program
	glBindVertexArray(0);
	glUseProgram(0);
}

// main function initializes glfw, creates window, initializes glew and app before main loop is started
int main(int argc, char *argv[])
{
	return context_setup_and_main_loop(640, 480, "cg1_pipeline_sample");
}
