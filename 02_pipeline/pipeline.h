#pragma once

#include <GL/glew.h>
#include <string>
#include <vector>
#include <iostream>
#include <cstdarg>

#ifdef CG1_SAMPLE_BASEPATH
	#define PREPEND_BASEPATH(path) (std::string(CG1_SAMPLE_BASEPATH"/")+path)
#else
	#define PREPEND_BASEPATH(path) (path)
#endif

// read shader of given type from given file
GLuint create_shader(const std::string& shader_file_name, GLenum shader_type)
{
	// read file to content string
	std::string content;
	FILE* fp = fopen(PREPEND_BASEPATH(shader_file_name).c_str(), "r");
	if (fp == 0) {
		std::cerr << "ERROR: could not read shader file " << shader_file_name << std::endl;
		return -1;
	}
	char buffer[4096];
	unsigned size, offset = 0;
	do {
		size = (unsigned)fread(buffer, 1, 4096, fp);
		content += std::string(buffer, buffer + size);
	} while (size == 4096);
	fclose(fp);
	// create shader 
	GLuint shader = glCreateShader(shader_type);
	const char* sources[1] = { content.c_str() };
	GLint       lengths[1] = { (GLint)content.size() };
	glShaderSource(shader, 1, sources, lengths);
	glCompileShader(shader);

	int result;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (result != 1) {
		std::cerr << "ERROR: compile error in shader " << shader_file_name << std::endl;

		GLint infologLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologLength);
		if (infologLength > 0) {
			int charsWritten = 0;
			char *infoLog = (char *)malloc(infologLength);
			glGetShaderInfoLog(shader, infologLength, &charsWritten, infoLog);
			std::cout << infoLog << std::endl;
			free(infoLog);
		}
		return -1;
	}
	return shader;
}

// create program from a list of shaders
// each shader is specified by a tripel consisting of shader_file_name:const char*, shader_type:GLenum
// and shader_id_ptr:GLuint*
// the list is terminated by a null ptr
GLuint create_program(const char* shader_file_name, ...)
{
	if (shader_file_name == 0)
		return -1;
	GLuint program = glCreateProgram();
	// declare va_list to access function parameters in ellipse (...)
	va_list list;
	// initialize list with parameter following shader_file_name parameter
	va_start(list, shader_file_name);
	// use the file name parameter to mark the end of shader specification
	while (shader_file_name != 0) {
		// extract shader type
		GLenum shader_type = va_arg(list, GLenum);
		// pointer to shader id
		GLuint* shader_id_ptr = va_arg(list, GLuint*);
		// create and attach shader
		*shader_id_ptr = create_shader(shader_file_name, shader_type);
		glAttachShader(program, *shader_id_ptr);
		// extract next shader file name parameter
		shader_file_name = va_arg(list, const char*);
	}
	// clean up ellipse parameters on function call stack
	va_end(list);
	// link program
	glLinkProgram(program);
	// check for linker errors
	GLint result = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&result);
	if (result != 1) {
		std::cerr << "link error in program of simple shader program:" << std::endl;
		// in case of error get and print out error message
		GLint logLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0) {
			std::vector<GLchar> infoLog(logLength);
			glGetProgramInfoLog(program, logLength, &logLength, &infoLog[0]);
			std::cout << &infoLog[0] << std::endl;
		}
		return -1;
	}
	// return shader program id
	return program;
}

bool set_shader_uniform(GLint prog_id, const std::string& name, GLfloat value)
{
	GLint variable_id = glGetUniformLocation(prog_id, name.c_str());
	if (variable_id == -1)
		return false;
	glUniform1f(variable_id, value);
	return true;
}

bool set_shader_uniform(GLint prog_id, const std::string& name, GLint value)
{
	GLint variable_id = glGetUniformLocation(prog_id, name.c_str());
	if (variable_id == -1)
		return false;
	glUniform1i(variable_id, value);
	return true;
}

// create array or element buffer
template <typename T>
GLuint create_buffer(const T* data_ptr, GLsizei count, bool is_array = true)
{
	// create buffer object for vertex data
	GLuint vbo;
	glGenBuffers(1, &vbo);
	// bind buffer and transfer data from CPU to GPU
	GLenum target = is_array ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;
	glBindBuffer(target, vbo);
	glBufferData(
		target,
		sizeof(T)*count,
		data_ptr,
		GL_STATIC_DRAW
	);
	return vbo;
}

// convenience function to create buffer from std::vector class
template <typename T>
GLuint create_buffer(const std::vector<T>& data_vector, bool is_array = true)
{
	return create_buffer(&data_vector.front(), GLsizei(data_vector.size()), is_array);
}

// create vertex array object for given program and interleaved buffer for case of position and color attribute
GLuint create_vertex_array_with_position_and_color(GLuint shader_program_id, GLuint vertex_buffer_id, int pos_dim, int col_dim)
{
	GLuint vertex_array_id;
	// create vertex array object
	glGenVertexArrays(1, &vertex_array_id);
	glBindVertexArray(vertex_array_id);
	// define binding of vertex attributes to vertex buffer in vao
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id);
	// set position attribute index according to layout specifier in vertex shader
	GLuint vPosition = 0;
	// alternatively one could query by name: 
	// vPosition = glGetAttribLocation(shader_program_id, "vPosition");

	// enable in vao the vPosition attribute
	glEnableVertexAttribArray(vPosition);
	// bind in vao the vPosition attribute to position data in vbo
	glVertexAttribPointer(
		vPosition,           // position attribute
		pos_dim,             // 3d or 4d positions
		GL_FLOAT,            // of float coordinate type 
		GL_FALSE,            // do not normalize
		(pos_dim+col_dim) * sizeof(GLfloat), // stride is offset to next vertex in bytes
		NULL                 // offset from beginning of vertex buffer
	);
	// set color attribute index according to layout specifier in vertex shader
	GLuint vColor = 1;
	// enable in vao the vPosition attribute
	glEnableVertexAttribArray(vColor);
	// bind in vao the vPosition attribute to position data in vbo
	glVertexAttribPointer(
		vColor,              // color attribute
		col_dim,             // 3d or 4d colors
		GL_FLOAT,            //       of float coordinate type
		GL_FALSE,            // do not normalize
		(pos_dim+col_dim) * sizeof(GLfloat), // stride is offset to next vertex in bytes
		(void*)(pos_dim * sizeof(GLfloat)) // offset from beginning of vertex buffer must be converted to pointer
	);
	// make vertex array slot empty
	glBindVertexArray(0);
	return vertex_array_id;
}
