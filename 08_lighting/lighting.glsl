#version 330

uniform vec3 sun_direction_eye = vec3(0.0, 1.0, 0.0);
uniform vec3 point_light_pos = vec3(2.0, 2.0, 0.0);
uniform float roughness = 0.5;
uniform int lighting_side = 0;
uniform int ambient_model = 0;
uniform int diffuse_model = 0;
uniform int specular_model = 0;
uniform bool physical_specular_models = true;

uniform mat4 viewMatrix;

uniform float weight_1 = 0.7;
uniform float weight_2 = 0.7;
uniform float weight_3 = 0.7;

const float PI = 3.14159265358979323846;

float oren_nayar_brdf(vec3 n, vec3 l, vec3 v)
{
	float roughsqaure = roughness * roughness;
	float vdotn = dot(v, n);
	float ldotn = dot(l, n);
	float sintan = sqrt((1.0 - vdotn * vdotn) * (1.0 - ldotn * ldotn)) / max(vdotn, ldotn);
	float anglediff = dot(normalize(v - n * vdotn), normalize(l - n * ldotn));
	float a = 1.0 - 0.5 * roughsqaure / (roughsqaure + 0.33);
	float b = 0.45 * roughsqaure / (roughsqaure + 0.09);
	return max(ldotn * (a + b * max(anglediff, 0.0) * sintan), 0.0);
}

float phong_specular_brdf(vec3 normal, vec3 omega_in, vec3 omega_out)
{
	vec3 omega_reflect = -reflect(omega_in, normal);
	float fs = dot(omega_reflect, omega_out);
	if (fs <= 0.0)
		return 0.0;
	float m = 1.0 / (roughness + 1.0 / 128.0);
	fs = pow(fs, m);
	if (physical_specular_models)
		fs *= (m+2.0)/(2.0*PI); 
	return fs;
}

float blinn_specular_brdf(vec3 normal, vec3 omega_in, vec3 omega_out)
{
	vec3 omega_half = normalize(omega_in + omega_out);
	float fs = dot(omega_half, normal);
	if (fs <= 0.0)
		return 0.0;
	float m = 1.0 / (roughness + 1.0 / 128.0);
	fs = pow(fs, m);
	if (physical_specular_models)
		fs *= (m+2.0)*(m+4.0)/(8.0*PI*(pow(2.0,-0.5*m)+m));
	return fs;
}

vec3 cooktorrance_specular_brdf(vec3 n, vec3 l, vec3 v, vec3 f0) 
{
	float m2 = roughness * roughness+0.0001;
	vec3 h = normalize(l + v);
	float nh = dot(n, h);
	float nl = dot(n, l);
	float nv = dot(n, v);
	float vh = dot(v, h);
	float cos2a = nh * nh;
	float tan2a = (1.0 - cos2a) / cos2a;
	float d = 1.0 / (m2 * cos2a * cos2a) * exp(-tan2a / m2);
	float g = clamp(min(2.0*nh*nv / vh, 2.0*nh*nl / vh), 0.0, 1.0);
	vec3 f = f0 + (vec3(1.0) - f0)*pow(1.0 - vh, 5.0);
	return d * f*g / (nv*PI);
}

vec3 local_lighting(vec3 color, vec3 n, vec3 l, vec3 v, vec3 L_in)
{
	vec3 result = vec3(0.0);
	float cosine_term = max(0.0, dot(n,l));
	switch (diffuse_model) {
	case 1 : result += color*L_in*cosine_term; break;
	case 2 : result += color*L_in*oren_nayar_brdf(n,l,v); break;
	}

	switch (specular_model) {
	case 1 : result += L_in*phong_specular_brdf(n,l,v)*(physical_specular_models?cosine_term:1.0); break;
	case 2 : result += L_in*blinn_specular_brdf(n,l,v)*(physical_specular_models?cosine_term:1.0); break;
	case 3 : result += L_in*cooktorrance_specular_brdf(n,l,v, vec3(0.8))*cosine_term; break;
	}
	return result;
}

// compute illumination from three lights: head, sun and point light
vec4 compute_illumination(in vec3 pos_eye, in vec3 nml_eye, in vec3 diffuse_color)
{
	vec3 result = vec3(0.0);
	vec3 omega_out = normalize(-pos_eye);
	if (dot(omega_out, nml_eye) > 0.0) {
		if (weight_1 > 0.0) {
			float tan_headlight_angle = length(omega_out.xy)/abs(omega_out.z);
			if (tan_headlight_angle < 0.4) {
				vec3 L_in_head = vec3(1.0,1.0,1.0);
				float dist_fraq = 1.0/length(pos_eye);
				L_in_head *= dist_fraq*dist_fraq;
				result += weight_1 * local_lighting(diffuse_color, nml_eye, omega_out, omega_out, L_in_head);
			}
		}
		// directional light from sun
		if (weight_2 > 0.0) {
			result += weight_2 * local_lighting(diffuse_color, nml_eye, sun_direction_eye, omega_out, vec3(1.0,1.0,0.95));
		}
		// point light source given in world coordinates
		if (weight_3 > 0.0) {
			vec4 point_light_hpos_eye = viewMatrix * vec4(point_light_pos, 1.0);
			vec3 point_light_pos_eye = point_light_hpos_eye.xyz / point_light_hpos_eye.w;
			vec3 omega_in = normalize(point_light_pos_eye - pos_eye);
			float dist_fraq = 1.0/length(point_light_pos_eye - pos_eye);
			vec3 L_point_light = vec3(1.0,0.5,1.0);
			L_point_light *= dist_fraq * dist_fraq;
			result += weight_3 * local_lighting(diffuse_color, nml_eye, omega_in, omega_out, L_point_light);
		}
	}
	if (ambient_model == 1)
		result += diffuse_color * (vec3(0.1) + 0.15*(weight_1 * vec3(1.0) + weight_2 * vec3(1.0,1.0,0.5) + weight_3*vec3(1.0,0.5,1.0)));
	return vec4(result, 1.0);
}
