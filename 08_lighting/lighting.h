#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "../04_viewing/viewing.h"

// lighting parameters
GLfloat gamma = 2.2f;
GLfloat sun_elevation = 90.0f;
glm::vec3 point_light_pos(2.0f, 2.0f, 0.0f);
GLfloat weight_1 = 0.7f;
GLfloat weight_2 = 0.7f;
GLfloat weight_3 = 0.7f;

GLint ambient_model = 1;
GLint diffuse_model = 1;
GLint specular_model = 1;
GLfloat roughness = 0.5f;
GLboolean physical_specular_models = GL_TRUE;
GLint lighting_side = 2;

bool check_lighting_key(int key, int mods, bool show_help = true)
{
	bool processed = false;
	switch (key) {
	case GLFW_KEY_Q: if (sun_elevation < 180) sun_elevation += 5; processed = true; break;
	case GLFW_KEY_Z: if (sun_elevation > 0) sun_elevation -= 5; processed = true; break;
	case GLFW_KEY_LEFT:  point_light_pos[0] += 0.1f; processed = true; break;
	case GLFW_KEY_RIGHT: point_light_pos[0] -= 0.1f; processed = true; break;
	case GLFW_KEY_UP:    point_light_pos[2] += 0.1f; processed = true; break;
	case GLFW_KEY_DOWN:  point_light_pos[2] -= 0.1f; processed = true; break;
	case GLFW_KEY_PAGE_UP:    point_light_pos[1] += 0.1f; processed = true; break;
	case GLFW_KEY_PAGE_DOWN:  point_light_pos[1] -= 0.1f; processed = true; break;
	case GLFW_KEY_Y:  physical_specular_models = !physical_specular_models; processed = true; break;
	case GLFW_KEY_F: if (gamma > 1.0f) gamma -= 0.2f; processed = true; break;
	case GLFW_KEY_G: if (gamma < 4.0f) gamma += 0.2f; processed = true; break;
	case GLFW_KEY_R: if (roughness > 0.95f) roughness = 0.0f; else roughness += 0.1f; processed = true; break;
	case GLFW_KEY_1: if (weight_1 > 0.95f) weight_1 = 0.0f; else weight_1 += 0.1f; processed = true; break;
	case GLFW_KEY_2: if (weight_2 > 0.95f) weight_2 = 0.0f; else weight_2 += 0.1f; processed = true; break;
	case GLFW_KEY_3: if (weight_3 > 0.95f) weight_3 = 0.0f; else weight_3 += 0.1f; processed = true; break;
	case GLFW_KEY_4: if (ambient_model == 1) ambient_model = 0; else ++ambient_model; processed = true; break;
	case GLFW_KEY_5: if (diffuse_model == 2) diffuse_model = 0; else ++diffuse_model; processed = true; break;
	case GLFW_KEY_6: if (specular_model == 3) specular_model = 0; else ++specular_model; processed = true; break;
	case GLFW_KEY_7: if (lighting_side == 2) lighting_side = 0; else ++lighting_side; processed = true; break;
	}
	if (show_help) {
		const char* ambient_model_names[] = { "none", "simple" };
		const char* diffuse_model_names[] = { "none", "lambertian", "oren nayar" };
		const char* specular_model_names[] = { "none", "phong", "blinn phong", "cook torrance" };
		const char* lighting_side_names[] = { "back face", "front face", "two sided" };
		std::cout << "<F|G> gamma         = " << gamma << std::endl;
		std::cout << "<Z|Q> sun_elevation = " << sun_elevation << std::endl;
		std::cout << "<Cursor> point_light   = " << point_light_pos[0] << "," << point_light_pos[1] << "," << point_light_pos[2] << std::endl;
		std::cout << "<1|2|3> head|sun|point= " << weight_1 << "|" << weight_2 << "|" << weight_3 << std::endl;
		std::cout << "<4> amient_model  = " << ambient_model_names[ambient_model] << std::endl;
		std::cout << "<5> diffuse_model = " << diffuse_model_names[diffuse_model] << std::endl;
		std::cout << "<6> specular_model= " << specular_model_names[specular_model] << std::endl;
		std::cout << "<R> roughness     = " << roughness << std::endl;
		std::cout << "<Y> physical_spec = " << (physical_specular_models?"true":"false") << std::endl;
		std::cout << "<7> lighting_side = " << lighting_side_names[lighting_side] << std::endl;
	}
	return processed;
}

void set_lighting_parameters(GLint prog_id, glm::mat4* model_matrix_ptr = 0)
{
	set_shader_uniform(prog_id, "gamma", gamma);
	set_shader_uniform(prog_id, "weight_1", weight_1);
	set_shader_uniform(prog_id, "weight_2", weight_2);
	set_shader_uniform(prog_id, "weight_3", weight_3);
	set_shader_uniform(prog_id, "ambient_model", ambient_model);
	set_shader_uniform(prog_id, "diffuse_model", diffuse_model);
	set_shader_uniform(prog_id, "specular_model", specular_model);
	set_shader_uniform(prog_id, "roughness", pow(roughness, 3.0f));
	set_shader_uniform(prog_id, "physical_specular_models", physical_specular_models);
	set_shader_uniform(prog_id, "lighting_side", lighting_side);

	GLint sun_direction_eye_id = glGetUniformLocation(prog_id, "sun_direction_eye");
	if (sun_direction_eye_id != -1) {
		float sun_angle = float(sun_elevation * M_PI / 180.0f);
		glm::vec3 sun_direction(cos(sun_angle), sin(sun_angle), 0.0f);
		glm::vec3 sun_direction_eye(get_model_view_matrix(model_matrix_ptr)*glm::vec4(sun_direction, 0.0f));
		sun_direction_eye = normalize(sun_direction_eye);
		glUniform3f(sun_direction_eye_id, sun_direction_eye[0], sun_direction_eye[1], sun_direction_eye[2]);
	}
	GLint point_light_pos_id = glGetUniformLocation(prog_id, "point_light_pos");
	if (point_light_pos_id != -1)
		glUniform3f(point_light_pos_id, point_light_pos[0], point_light_pos[1], point_light_pos[2]);
}