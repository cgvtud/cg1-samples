#define _USE_MATH_DEFINES
#include <cmath>
#include "../01_context/context.h"
#include "../02_pipeline/pipeline.h"
#include "../03_depth_buffer/random_scene.h"
#include "../04_viewing/viewing.h"
#include "../06_geometry_shader/normals.h"
#include "instanced.h"

#define RESTART_INDEX GLint(-1)

// flat shader
GLuint vertex_shader_id = -1, fragment_shader_id = -1, shader_program_id = -1;
GLint  color_mode_id = -1;

// normal shader
GLuint normal_vs_id = -1, normal_gs_id = -1, normal_fs_id = -1, normal_sp_id = -1;
GLuint normal_length_id = -1;

// tesselation parameters
int N = 128, M = 128;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
		return;
	std::cout << "------------------------------------" << std::endl;
	bool processed = check_surface_key(key, mods, false);
	processed = check_instanced_geometry_key(key, mods) || processed;
	processed = check_normal_key(key, mods) || processed;
	if (!processed) {
		switch (key) {
		case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE); break;
		}
	}
	const char* color_mode_names[] = { "color", "position", "tex coords", "normal" };
	std::cout << "<C|P|T|N> color_mode = " << color_mode_names[color_mode] << std::endl;
	std::cout << "------------------------------------" << std::endl;
}

// prepare context and render objects
void init(GLFWwindow* window, int w, int h)
{
	// define color to clear the color buffer in the glClear() call in draw() function
	glClearColor(0.1f, 0.3f, 1.0f, 1.0f);
	glClearDepth(1.0f);

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	shader_program_id = create_program(
		"flat_shader.glvs", GL_VERTEX_SHADER, &vertex_shader_id,
		"../05_indexed/flat_shader.glfs", GL_FRAGMENT_SHADER, &fragment_shader_id, 0);

	normal_sp_id = create_program(
		"normal_shader.glvs", GL_VERTEX_SHADER, &normal_vs_id,
		"../06_geometry_shader/normal_shader.glgs", GL_GEOMETRY_SHADER, &normal_gs_id,
		"../06_geometry_shader/normal_shader.glfs", GL_FRAGMENT_SHADER, &normal_fs_id, 0);

	init_viewing(window, w, h, shader_program_id, normal_sp_id);

	glfwSetKeyCallback(window, key_callback);

	color_mode_id = glGetUniformLocation(shader_program_id, "color_mode");
	normal_length_id = glGetUniformLocation(normal_sp_id, "normal_length");

	glPrimitiveRestartIndex(RESTART_INDEX);
	glEnable(GL_PRIMITIVE_RESTART);

	create_instanced_geometry(N, M, RESTART_INDEX);
}

// delete render objects
void cleanup()
{
	if (shader_program_id != -1)
		glDeleteProgram(shader_program_id);
	if (vertex_shader_id != -1)
		glDeleteShader(vertex_shader_id);
	if (fragment_shader_id != -1)
		glDeleteShader(fragment_shader_id);
	if (normal_sp_id != -1)
		glDeleteProgram(normal_sp_id);
	if (normal_vs_id != -1)
		glDeleteShader(normal_vs_id);
	if (normal_gs_id != -1)
		glDeleteShader(normal_gs_id);
	if (normal_fs_id != -1)
		glDeleteShader(normal_fs_id);

	cleanup_instanced_geometry();
}

void display()
{
	// clear the buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// validity check for shader and vertex array
	if (shader_program_id == -1)
		return;

	// enable shader program
	glUseProgram(shader_program_id);
	glUniform1i(color_mode_id, color_mode);

	draw_geometry(shader_program_id, false, nr_instances);

	if (show_normals && normal_sp_id != -1) {
		glUseProgram(normal_sp_id);
		// normal length is defined in object coordinates and needs to be scaled
		// for mesh as mesh is rendered with model view that uniformly scales and 
		// translates mesh such that maximum extent maps to [-1, 1]
		glUniform1f(normal_length_id, 
			object_mode == 0 ? normal_length : 
			                   normal_length*mesh_scale);
		draw_geometry(normal_sp_id, true, nr_instances);
	}
	glUseProgram(0);
}

// main function initializes glfw, creates window, initializes glew and app before main loop is started
int main(int argc, char *argv[])
{
	return context_setup_and_main_loop(1024, 768, "cg1_instanced_sample");
}
