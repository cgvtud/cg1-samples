#pragma once

#include "../05_indexed/geometry.h"

// instancing parameters
int nr_instances = 10;
int max_nr_instances = 500;
std::vector<glm::vec3> instance_offsets;
GLuint instance_offset_vbo_id = -1;

bool check_instanced_geometry_key(int key, int mods, bool show_help = true)
{
	bool processed = check_geometry_key(key, mods, show_help);
	if (!processed) {
		switch (key) {
		case GLFW_KEY_F5: if (nr_instances < max_nr_instances) nr_instances += 1; processed = true; break;
		case GLFW_KEY_F4: if (nr_instances > 1) nr_instances -= 1; processed = true; break;
		}
	}
	if (show_help)
		std::cout << "<F4|F5> nr_instances  = " << nr_instances << std::endl;
	return processed;
}

const mesh_info* create_instanced_geometry(int N, int M, GLint restart_index)
{
	const mesh_info* mi_ptr = create_geometry(N, M, restart_index);
	// construct instance offsets for mesh
	int I = 0, i = 0, d = 0, o = 0, y = 0, n = 1, d_max = 0, y_max = 1;
	while (I < max_nr_instances) {
		float angle = float((i + 0.5f*o) * 2 * M_PI / n);
		glm::vec3 offset(0.5f*d*cos(angle), 0.5f*d*sin(angle), -y);
		instance_offsets.push_back(mesh_scale*offset);
		++i;
		++I;
		if (i == n) {
			++y;
			i = 0;
			if (y == y_max) {
				d_max += 2;
				y_max = d_max + 1;
				d = d_max;
				n = 3 * d;
				y = 0;
			}
			else {
				o = 1 - o;
				d -= 1;
				n = d == 0 ? 1 : 3 * d;
			}
		}
	}
	// construct instance offsets for height field
	I = 1; n = 2; i = 0; o = 0;
	static glm::vec3 dirs[4] = { {1, 0, 0}, {0, 0, 1}, {-1, 0, 0}, {0, 0, -1} };
	instance_offsets.push_back(glm::vec3(0.0f));
	while (I < max_nr_instances) {
		instance_offsets.push_back(
			float(n)*(dirs[(o + 3) % 4] + dirs[o]) + float(2 * (i + 1))* dirs[(o + 1) % 4]
		);
		++I;
		if (++i == n) {
			i = 0;
			if (++o == 4) {
				o = 0;
				n += 2;
			}
		}
	}
	// transfer all instance offsets to vbo
	instance_offset_vbo_id = create_buffer(instance_offsets);

	// add instance offset attribute to vaos
	// for height field set instance attribute to zero vector
	glm::vec3 zero_offset(2.0f, 0.0f, 0.0f);
	GLint vInstanceOffset = 4;
	glBindVertexArray(mesh_vao_id);
	glBindBuffer(GL_ARRAY_BUFFER, instance_offset_vbo_id);
	glEnableVertexAttribArray(vInstanceOffset);
	glVertexAttribPointer(vInstanceOffset, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), NULL);
	glVertexAttribDivisor(vInstanceOffset, 1);
	glBindVertexArray(hfield_vao_id);
	glEnableVertexAttribArray(vInstanceOffset);
	glVertexAttribPointer(vInstanceOffset, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3),
		(void*)(max_nr_instances * sizeof(glm::vec3)));
	glVertexAttribDivisor(vInstanceOffset, 1);
	glVertexAttrib3fv(vInstanceOffset, glm::value_ptr(zero_offset));
	glBindVertexArray(0);

	return mi_ptr;
}

void cleanup_instanced_geometry()
{
	cleanup_geometry();
	if (instance_offset_vbo_id != -1)
		glDeleteBuffers(1, &instance_offset_vbo_id);
}