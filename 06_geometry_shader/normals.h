#pragma once

bool show_normals = true;
float normal_length = 0.01f;

bool check_normal_key(int key, int mods, bool show_help = true)
{
	bool processed = false;
	switch (key) {
	case GLFW_KEY_F2: normal_length *= 0.75f; processed = true; break;
	case GLFW_KEY_F3: normal_length *= 1.0f / 0.75f; processed = true; break;
	case GLFW_KEY_F1: show_normals = !show_normals; processed = true; break;
	}
	if (show_help) {
		std::cout << "<F2|F3> normal_length = " << normal_length << std::endl;
		std::cout << "<F1> show_normals = " << (show_normals ? "true":"false") << std::endl;
	}
	return processed;
}

