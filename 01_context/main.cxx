#include "context.h"

// prepare context and render objects
void init(GLFWwindow* window, int w, int h)
{
	// define color to clear the color buffer in the glClear() call in draw() function
	glClearColor(1.0f, 0.5f, 0.7f, 1.0f);
	// define viewport from window extent
	glViewport(0, 0, w, h);
}

// we do not have anything to clear yet
void cleanup()
{
}

// only clear the background with the color defined in the init method
void display()
{
	// clear the buffers
	glClear(GL_COLOR_BUFFER_BIT);
}

// main function initializes glfw, creates window, initializes glew and app before main loop is started
int main(int argc, char *argv[])
{
	return context_setup_and_main_loop(640, 480);
}
