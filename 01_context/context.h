#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <iostream>

// prepare context and render objects
extern void init(GLFWwindow* window, int w, int h);
// delete opengl objects
extern void cleanup();
// drawing each frame
extern void display();

// callback to handle debug messages
void APIENTRY debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	std::cerr << "OpenGL debug message: " << std::string(message, length) << std::endl;
}

// content of main function that sets op initializes glfw, creates window, initializes glew and app before main loop is started
int context_setup_and_main_loop(int w, int h, const std::string& name = "cg1_context_sample")
{
	// init glfw
	if (glfwInit() == GLFW_FALSE)
		return -2;

	// create window and its OpenGL context
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	GLFWwindow* window = glfwCreateWindow(w, h, name.c_str(), NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -3;
	}

	// Make the window's context current
	glfwMakeContextCurrent(window);

	// init glew with a current context that is providing opengl version
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
		return -1;
	}
	std::cout << "OpenGL version:" << glGetString(GL_VERSION) << std::endl;
	glEnable(GL_DEBUG_OUTPUT); // not necessary as on by default
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(debug_callback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	// init opengl objects
	glfwGetWindowSize(window, &w, &h);
	init(window, w, h);

	// main loop until the user closes the window
	while (!glfwWindowShouldClose(window)) {

		// draw everything with opengl		
		display();

		// Swap front and back buffers
		glfwSwapBuffers(window);

		// wait for an event
		glfwWaitEvents();

		// poll for and process events
		glfwPollEvents();
	}
	// destroy opengl objects and terminate glfw
	cleanup();
	glfwTerminate();
	return 0;
}
