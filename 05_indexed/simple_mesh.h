#pragma once

#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <sstream>
#include <fstream>
#include "surface.h"

#ifdef CG1_SAMPLE_BASEPATH
	#define PREPEND_BASEPATH(path) (std::string(CG1_SAMPLE_BASEPATH"/")+path)
#else
	#define PREPEND_BASEPATH(path) (path)
#endif

class simple_mesh 
{
protected:
	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> tex_coords;
	std::vector<glm::vec3> normals;
	std::vector<GLint> position_indices;
	std::vector<GLint> tex_coord_indices;
	std::vector<GLint> normal_indices;
	std::vector<GLint> faces;
public:
	/// create a new empty face to which new corners are added and return face index
	GLint start_face(int delta = 0)
	{
		faces.push_back((GLint)position_indices.size() + delta);
		return GLint(faces.size() - 1);
	}
	/// create a new corner from position, optional normal and optional tex coordinate indices and return corner index
	GLint new_corner(GLint position_index, GLint tex_coord_index = -1, GLint normal_index = -1)
	{
		position_indices.push_back(position_index);
		if (tex_coord_index != -1)
			tex_coord_indices.push_back(tex_coord_index);
		if (normal_index != -1)
			normal_indices.push_back(normal_index);
		return GLint(position_indices.size() - 1);
	}
	/// 
	void copy_corner(int c0, int c1 = 0)
	{
		c0 += int(position_indices.size());
		c1 += int(position_indices.size());
		if (c1 >= int(position_indices.size()))
			new_corner(position_indices[c0], tex_coord_indices[c0], normal_indices[c0]);
		else {
			position_indices[c1] = position_indices[c0];
			tex_coord_indices[c1] = tex_coord_indices[c0];
			normal_indices[c1] = normal_indices[c0];
		}
	}
	/// return the number of faces
	GLint get_nr_faces() const { return GLint(faces.size()); }
	/// return index of first corner of face with index fi
	GLint begin_corner(GLint fi) const { return faces[fi]; }
	/// return index of end corner (one after the last one) of face with index fi
	GLint end_corner(GLint fi) const { return fi + 1 == faces.size() ? GLint(position_indices.size()) : faces[fi + 1]; }
	/// return number of edges/corners of face with index fi
	GLint face_degree(GLint fi) const { return end_corner(fi) - begin_corner(fi); }
	/// merge the three indices into one index into a vector of unique index triples
	void merge_indices(std::vector<GLint>& vertex_indices, std::vector<glm::ivec3>& unique_triples) const
	{
		std::map<std::tuple<GLint, GLint, GLint>, GLint> corner_to_index;
		for (GLint ci = 0; ci < GLint(position_indices.size()); ++ci) {
			// construct corner
			glm::ivec3 c(position_indices[ci],
				ci < GLint(tex_coord_indices.size()) ? tex_coord_indices[ci] : 0,
				ci < GLint(normal_indices.size()) ? normal_indices[ci] : 0);
			std::tuple<GLint, GLint, GLint> triple(c[0], c[1], c[2]);
			// look corner up in map
			auto iter = corner_to_index.find(triple);
			// determine vertex index
			GLint vi;
			if (iter == corner_to_index.end()) {
				vi = GLint(unique_triples.size());
				corner_to_index[triple] = vi;
				unique_triples.push_back(c);
			}
			else
				vi = iter->second;

			vertex_indices.push_back(vi);
		}
	}
	/// extract element array buffers for triangulation
	void extract_triangle_element_buffer(const std::vector<GLint>& vertex_indices, std::vector<GLint>& triangle_element_buffer) const
	{
		// construct triangle element buffer
		for (GLint fi = 0; fi < GLint(faces.size()); ++fi) {
			if (face_degree(fi) == 3) {
				for (GLint ci = begin_corner(fi); ci < end_corner(fi); ++ci)
					triangle_element_buffer.push_back(vertex_indices.at(ci));
			}
			else {
				// in case of non triangular faces do simplest triangulation approach that assumes convexity of faces
				for (GLint ci = begin_corner(fi) + 2; ci < end_corner(fi); ++ci) {
					triangle_element_buffer.push_back(vertex_indices.at(begin_corner(fi)));
					triangle_element_buffer.push_back(vertex_indices.at(ci - 1));
					triangle_element_buffer.push_back(vertex_indices.at(ci));
				}
			}
		}
	}
	/// parse two float from string 
	glm::vec2 extract_vec2(const char* buffer)
	{
		std::string s(buffer);
		std::stringstream ss(s);
		glm::vec2 v(0.0f);
		ss >> v[0] >> v[1];
		return v;
	}
	/// parse three float from string 
	glm::vec3 extract_vec3(const char* buffer)
	{
		std::string s(buffer);
		std::stringstream ss(s);
		glm::vec3 v(0.0f);
		ss >> v[0] >> v[1] >> v[2];
		return v;
	}
	/// extract corner information recursively
	void extract_corners(const std::string& s)
	{
		std::string corner;
		size_t space_loc = s.find_first_of(' ');
		if (space_loc != std::string::npos)
			corner = s.substr(0, space_loc);
		else
			corner = s;

		size_t sep_loc = corner.find_first_of('/');
		if (sep_loc == std::string::npos) {
			std::stringstream ss(corner);
			GLuint idx;
			ss >> idx;
			if (!ss.fail())
				new_corner(idx-1, idx-1, idx-1);
		}
		else {
			corner.replace(sep_loc, 1, 1, ' ');
			sep_loc = corner.find_first_of('/', sep_loc);
			if (sep_loc != std::string::npos)
				corner.replace(sep_loc, 1, 1, ' ');
			std::stringstream ss(corner);
			GLuint pi, ti, ni;
			ss >> pi >> ti;
			if (!ss.fail()) {
				ss >> ni;
				if (ss.fail())
					new_corner(pi - 1, ti - 1, pi - 1);
				else
					new_corner(pi - 1, ti - 1, ni - 1);
			}
		}
		if (space_loc != std::string::npos)
			extract_corners(s.substr(space_loc + 1));
	}
	/// compute per vertex normals from corner positions
	void compute_per_vertex_normals()
	{
		normals.resize(positions.size(), glm::vec3(0.0f));
		for (int fi = 0; fi < int(faces.size()); ++fi) {
			int cb = faces[fi];
			int ce = (fi == faces.size() - 1 ? int(positions.size()) : faces[fi + 1]);
			int ci, cp = ce - 1;
			const glm::vec3& p0 = positions[position_indices[cb]];
			glm::vec3 nml(0.0f);
			for (ci = cb; ci < ce; cp=ci, ++ci)
				nml += cross(positions[position_indices[cp]] - p0, positions[position_indices[ci]] - p0);
			nml = normalize(nml);
			for (ci = cb; ci < ce; ++ci)
				normals[normal_indices[ci]] += nml;
		}
		for (auto& n : normals)
			n = normalize(n);
	}
	/// read simple mesh from file
	bool read(const std::string& file_name)
	{
		std::ifstream is(PREPEND_BASEPATH(file_name));
		if (is.fail())
			return false;
		char buffer[2048];
		while (!is.eof()) {
			is.getline(buffer, 2048);
			if (buffer[0] == 0)
				continue;
			switch (buffer[0]) {
			case 'v':
				switch (buffer[1]) {
				case ' ': positions.push_back(extract_vec3(buffer + 2)); break;
				case 't': tex_coords.push_back(extract_vec2(buffer + 3)); break;
				case 'n': normals.push_back(extract_vec3(buffer + 3)); break;
				}
				break;
			case 'f':
				start_face();
				extract_corners(std::string(buffer + 2));
				switch (position_indices.size() - faces.back()) {
				case 3: // nothing to be done
					break;
				case 4 :                 // a b c d
					start_face(-1);      // a b c | d
					copy_corner(-2);     // a b c | d c
					copy_corner(-2);     // a b c | d c d
					copy_corner(-6, -3); // a b c | a c d
					break;
				case 5:                  // a b c d e
					start_face(-2);      // a b c | d e
					copy_corner(-2);     // a b c | d e d
					start_face();        // a b c | d e d |
					copy_corner(-6);     // a b c | d e d | a
					copy_corner(-2);     // a b c | d e d | a d
					copy_corner(-4);     // a b c | d e d | a d e
					copy_corner(-3, -6); // a b c | a e d | a d e
					copy_corner(-7, -5); // a b c | a c d | a d e
					break;
				default:
					std::cerr << "not implemented triangulation of polygon" << std::endl;
					exit(-1);
					break;
				}
			}
		}
		if (normals.empty())
			compute_per_vertex_normals();
		return true;
	}
	/// compute center and extent of bounding box
	void compute_bounding_box(glm::vec3& center, glm::vec3& extent)
	{
		glm::vec3 min_p = positions[0], max_p = positions[0];
		for (const auto& p : positions) {
			min_p = min(min_p, p);
			max_p = max(max_p, p);
		}
		center = 0.5f*(min_p + max_p);
		extent = max_p - min_p;
	}
	/// extract vertex attribute array, return size of color in bytes
	void extract_vertex_attribute_buffer(
		const std::vector<GLint>& vertex_indices,
		const std::vector<glm::ivec3>& unique_triples,
		std::vector<surface_vertex>& attrib_buffer) const
	{
		attrib_buffer.resize(unique_triples.size());
		surface_vertex* vertex_ptr = &attrib_buffer.front();
		for (auto t : unique_triples) {
			vertex_ptr->position  = positions[t[0]];
			vertex_ptr->tex_coord = tex_coords[t[1]];
			vertex_ptr->normal    = normals[t[2]];
			++vertex_ptr;
		}
	}
};

// read mesh from file in order to create render objects
bool create_mesh_render_objects(const std::string& file_name,
	GLsizei& nr_vertices, GLsizei& nr_elements, glm::vec3& center, float& scale,
	GLuint& vbo_id, GLuint& ebo_id, GLuint& vao_id)
{
	simple_mesh mesh;
	if (!mesh.read(file_name))
		return false;
	glm::vec3 extent;
	mesh.compute_bounding_box(center, extent);
	scale = std::max(extent[0], std::max(extent[1], extent[2]));
	std::vector<GLint> vertex_indices;
	std::vector<glm::ivec3> unique_triples;
	std::vector<surface_vertex> vertex_data;
	std::vector<GLint> triangle_element_buffer;
	mesh.merge_indices(vertex_indices, unique_triples);
	mesh.extract_triangle_element_buffer(vertex_indices, triangle_element_buffer);
	nr_elements = GLsizei(vertex_indices.size());
	mesh.extract_vertex_attribute_buffer(vertex_indices, unique_triples, vertex_data);
	nr_vertices = GLsizei(vertex_data.size());
	vbo_id = create_buffer(vertex_data, true);
	ebo_id = create_buffer(vertex_indices, false);
	vao_id = create_surface_vertex_array(vbo_id, ebo_id);
	return true;
}
