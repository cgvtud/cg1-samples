#pragma once

#define _USE_MATH_DEFINES
#include <math.h>
#include "../04_viewing/viewing.h"
#include "simple_mesh.h"
#include "height_field.h"

GLsizei hfield_nr_vertices;
GLsizei hfield_nr_elements;
GLuint hfield_vbo_id = -1;
GLuint hfield_ebo_id = -1;
GLuint hfield_cbo_id = -1;
GLuint hfield_vao_id = -1;

bool have_mesh = false;
GLsizei mesh_nr_vertices;
GLsizei mesh_nr_elements;
GLuint mesh_vbo_id = -1;
GLuint mesh_ebo_id = -1;
GLuint mesh_vao_id = -1;

glm::vec3 mesh_center(0, 0, 0);
float mesh_scale = 1;

// two object modes: 0 ... height field, 1 ... mesh
int object_mode = 0;

// information provided per mesh
struct mesh_info
{
	std::string obj_file_name;
	std::string diffuse_tex_file_name;
	int diffuse_W, diffuse_H, diffuse_channels;
	std::string normal_tex_file_name;
	int normal_W, normal_H, normal_channels;
	std::string displacement_tex_file_name;
	int displacement_W, displacement_H, displacement_channels;
};

bool check_geometry_key(int key, int mods, bool show_help = true)
{
	bool processed = false;
	switch (key) {
	case GLFW_KEY_H: object_mode = 0; processed = true; break;
	case GLFW_KEY_M: object_mode = 1; processed = true; break;
	}
	if (show_help) {
		const char* object_mode_names[] = { "height field", "mesh" };
		std::cout << "<H|M> object_mode   = " << object_mode_names[object_mode] << std::endl;
	}
	return processed;
}

// create geometry of height field and read mesh based on default settings 
// and the content of the file "model.txt" in the working directory
// possible contents to select a model different from the default trex model
// are "apple" and "walnut"
const mesh_info* create_geometry(int N, int M, GLint restart_index)
{
	static mesh_info mesh_infos[3] = {
		{ "../models/apple.obj",
		  "../models/apple_diffuse_1024_rgb.raw", 1024, 1024, 3,
		  "../models/apple_normal_1024_rgb.raw", 1024, 1024, 3,
		  "", 0, 0, 0
		},
		{ "../models/walnut.obj",
		  "../models/walnut_diffuse_1024_rgb.raw", 1024, 1024, 3,
		  "../models/walnut_normal_1024_rgb.raw", 1024, 1024, 3,
		  "", 0, 0, 0
		},
		{ "../models/trex.obj",
		  "../models/trex_diffuse_2048_rgb.raw", 2048, 2048, 3,
		  "../models/trex_normal_2048_rgb.raw", 2048, 2048, 3,
		  "../models/trex_displacement_2048_y.raw", 2048, 2048, 1
		}
	};
	create_height_field_render_objects(N, M, restart_index,
		hfield_nr_vertices, hfield_nr_elements, hfield_vbo_id, hfield_cbo_id,
		hfield_ebo_id, hfield_vao_id);
	std::string mesh_file_name;
	int mesh_index = 2;
	std::ifstream is("model.txt");
	if (!is.fail()) {
		std::string mesh_name;
		is >> mesh_name;
		if (mesh_name == "apple")
			mesh_index = 0;
		else if (mesh_name == "walnut")
			mesh_index = 1;
	}
	have_mesh = create_mesh_render_objects(mesh_infos[mesh_index].obj_file_name,
		mesh_nr_vertices, mesh_nr_elements, mesh_center, mesh_scale,
		mesh_vbo_id, mesh_ebo_id, mesh_vao_id);

	return mesh_infos + mesh_index;
}

void cleanup_geometry()
{
	if (hfield_vbo_id != -1)
		glDeleteBuffers(1, &hfield_vbo_id);
	if (hfield_ebo_id != -1)
		glDeleteBuffers(1, &hfield_ebo_id);
	if (hfield_cbo_id != -1)
		glDeleteBuffers(1, &hfield_cbo_id);
	if (hfield_vao_id != -1)
		glDeleteVertexArrays(1, &hfield_vao_id);
	if (mesh_vbo_id != -1)
		glDeleteBuffers(1, &mesh_vbo_id);
	if (mesh_ebo_id != -1)
		glDeleteBuffers(1, &mesh_ebo_id);
	if (mesh_vao_id != -1)
		glDeleteVertexArrays(1, &mesh_vao_id);
}

// draw scene as vertices only for normal illustration or otherwise with element buffers
void draw_geometry(GLint prog_id, bool vertices = false, int nr_instances = 1, bool patches = false)
{
	if (object_mode == 1 && have_mesh) {
		// compute model matrix
		glm::mat4 model_matrix = glm::translate(
			glm::scale(
				glm::rotate(
					glm::identity<glm::mat4>(),
					float(0.5*M_PI), glm::vec3(1.0f,0.0f,0.0f)),
				glm::vec3(2.0f / mesh_scale)),
			-mesh_center
		);
		///
		set_viewing_parameters(prog_id, &model_matrix);
		// activate vertex array object to specify where vertex data comes from
		glBindVertexArray(mesh_vao_id);
		// in case of mesh set the color attribute once for all vertices
		glVertexAttrib4f(3, 0.5f, 0.5f, 0.5f, 1.0f);
		if (vertices) {
			GLint element_buffer_id;
			glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &element_buffer_id);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			if (nr_instances == 1)
				glDrawArrays(GL_POINTS, 0, mesh_nr_vertices);
			else
				glDrawArraysInstanced(GL_POINTS, 0, mesh_nr_vertices, nr_instances);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_id);
		}
		else {
			// finally draw the vertex data
			if (nr_instances == 1)
				glDrawElements(patches ? GL_PATCHES : GL_TRIANGLES, mesh_nr_elements, GL_UNSIGNED_INT, 0);
			else
				glDrawElementsInstanced(patches ? GL_PATCHES : GL_TRIANGLES, mesh_nr_elements, GL_UNSIGNED_INT, 0, nr_instances);
		}
	}
	else {
		///
		set_viewing_parameters(prog_id);
		// activate vertex array object to specify where vertex data comes from
		glBindVertexArray(hfield_vao_id);
		// incase of rendering with one point per vertex, ignore element buffer
		if (vertices) {
			GLint element_buffer_id;
			glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &element_buffer_id);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			// and draw arrays directly
			if (nr_instances == 1)
				glDrawArrays(GL_POINTS, 0, hfield_nr_vertices);
			else
				glDrawArraysInstanced(GL_POINTS, 0, hfield_nr_vertices, nr_instances);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_id);
		}
		else {
			// finally draw the vertex data
			if (nr_instances == 1)
				glDrawElements(GL_TRIANGLE_STRIP, hfield_nr_elements, GL_UNSIGNED_INT, 0);
			else
				glDrawElementsInstanced(GL_TRIANGLE_STRIP, hfield_nr_elements, GL_UNSIGNED_INT, 0, nr_instances);
		}
	}
	// deactivate vertex array and shader program
	glBindVertexArray(0);
}
