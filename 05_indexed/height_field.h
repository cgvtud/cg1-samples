#pragma once 

#include <vector>
#include <random>
#include "../02_pipeline/pipeline.h"
#include "surface.h"

std::vector<float> height_field;

int linear_index(int i, int j, int n, int m)
{
	return (j%m) * n + (i%n);
}

void construct_height_field(std::vector<float>& heights, int n, int m)
{
	int di = n, dj = m;
	heights.resize(n*m);
	heights[linear_index(0, 0, n, m)] = 0;
	float amplitude = 0.5f;
	std::default_random_engine e;
	std::uniform_real_distribution<float> u(-1, 1);
	while (di > 1 || dj > 1) {
		if (di >= dj) {
			di /= 2;
			for (int j = 0; j < m; j += dj) {
				for (int i = di; i < n; i += 2 * di) {
					heights[linear_index(i, j, n, m)] = 0.5f * (
						heights[linear_index(i - di, j, n, m)] +
						heights[linear_index(i + di, j, n, m)]
						) + amplitude * u(e);
				}
			}
		}
		else {
			dj /= 2;
			for (int i = 0; i < n; i += di) {
				for (int j = dj; j < m; j += 2 * dj) {
					heights[linear_index(i, j, n, m)] = 0.5f * (
						heights[linear_index(i, j - dj, n, m)] +
						heights[linear_index(i, j + dj, n, m)]
						) + amplitude * u(e);
				}
			}
		}
		amplitude *= 0.7f;
	}
}

glm::u8vec4 get_height_field_color(float h)
{
	if (h < 0)
		return glm::u8vec4(0, 0, 255 + uint8_t(255*h), 255);
	if (h < 0.1f)
		return glm::u8vec4(uint8_t(255 * 3 * h), uint8_t(255*(1 - 5 * h)), 0, 255);
	if (h < 0.3f)
		return glm::u8vec4(uint8_t(255*(h + 0.2f)), 128, uint8_t(255*2.5f*(h - 0.1f)), 255);
	if (h < 0.5f)
		return glm::u8vec4(uint8_t(255*(0.2f + h)), 255*(0.2f + h), 255*(0.2f + h), 255);
	return glm::u8vec4(255, 255, 255, 255);
}

void construct_height_field_vertices(
	const std::vector<float>& heights, int n, int m, 
	std::vector<surface_vertex>& vertices, std::vector<glm::u8vec4>* colors_ptr = 0)
{
	float su = 1.0f / n, sv = 1.0f / m;
	float sx = 2 * su, sy = 2 * sv;
	for (int j = 0; j <= m; ++j)
		for (int i = 0; i <= n; ++i) {
			float h = heights[linear_index(i, j, n, m)];
			float hpx = heights[linear_index(i + 1, j, n, m)];
			float hmx = heights[linear_index(i + n - 1, j, n, m)];
			float hx = (hpx - hmx)/(2*sx);
			float hpy = heights[linear_index(i, j + 1, n, m)];
			float hmy = heights[linear_index(i, j + m - 1, n, m)];
			float hy = (hpy - hmy)/(2*sy);
			glm::vec2 tex_coord(su*i, sv*j);
			glm::vec3 position(sx*i - 1, h, sy*j - 1);
			glm::vec3 normal(-hx, 1, -hy);
			normal = normalize(normal);
			vertices.push_back(surface_vertex({ position, tex_coord, normal }));
			if (colors_ptr)
				colors_ptr->push_back(get_height_field_color(h));
		}
}

GLuint create_regular_grid_element_buffer(
	std::vector<GLint>& indices, GLsizei& nr_elements,
	int n, int m, GLint restart_index = -1)
{
	for (int j = 1; j <= m; ++j) {
		for (int i = 0; i <= n; ++i) {
			indices.push_back(linear_index(i, j - 1, n + 1, m + 1));
			indices.push_back(linear_index(i, j, n + 1, m + 1));
		}
		indices.push_back(restart_index);
	}
	// create buffer object for vertex data
	GLuint ebo;
	glGenBuffers(1, &ebo);
	nr_elements = GLsizei(indices.size());
	// bind buffer and transfer data from CPU to GPU
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		sizeof(GLint)*nr_elements,
		&indices.front(),
		GL_STATIC_DRAW
	);
	return ebo;
}

void create_height_field_render_objects(int n, int m, GLint restart_index,
	GLsizei& nr_vertices, GLsizei& nr_elements, GLuint& vbo_id, GLuint& cbo_id, GLuint& ebo_id, GLuint& vao_id)
{
	construct_height_field(height_field, n, m);
	std::vector<surface_vertex> height_field_vertices;
	std::vector<glm::u8vec4> height_field_colors;
	construct_height_field_vertices(height_field, n, m, height_field_vertices, &height_field_colors);
	nr_vertices = GLsizei(height_field_vertices.size());
	std::vector<GLint> height_field_indices;
	create_regular_grid_element_buffer(height_field_indices, nr_elements, n, m, restart_index);
	vbo_id = create_buffer(height_field_vertices);
	cbo_id = create_buffer(height_field_colors);
	ebo_id = create_buffer(height_field_indices, false);
	vao_id = create_surface_vertex_array(vbo_id, ebo_id, cbo_id);
}