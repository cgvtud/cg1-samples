#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

// four color modes: 0 ... color, 1 ... positions, 2 ... tex coords, 3 ... normals
int color_mode = 0;

bool check_surface_key(int key, int mods, bool show_help = true)
{
	bool processed = false;
	switch (key) {
	case GLFW_KEY_C: color_mode = 0; processed = true; break;
	case GLFW_KEY_P: color_mode = 1; processed = true; break;
	case GLFW_KEY_T: color_mode = 2; processed = true; break;
	case GLFW_KEY_N: color_mode = 3; processed = true; break;
	}
	if (show_help) {
		const char* color_mode_names[] = { "color", "position", "tex coords", "normal" };
		std::cout << "<C|P|T|N> color_mode = " << color_mode_names[color_mode] << std::endl;
	}
	return processed;
}

struct surface_vertex
{
	glm::vec3 position;
	glm::vec2 tex_coord;
	glm::vec3 normal;
};

GLuint create_surface_vertex_array(GLint vertex_buffer_id, GLint element_buffer_id, GLint color_buffer_id = -1)
{
	GLuint vao_id;
	// create vertex array object
	glGenVertexArrays(1, &vao_id);
	glBindVertexArray(vao_id);
	// bind element buffer to corresponding slot
	if (element_buffer_id != -1)
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_id);
	// define binding of vertex attributes to vertex buffer in vao
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id);
	// set position attribute index according to layout specifier in vertex shader
	GLuint vPosition = 0;
	// alternatively one could query by name: 
	// vPosition = glGetAttribLocation(shader_program_id, "vPosition");

	// enable in vao the vPosition attribute
	glEnableVertexAttribArray(vPosition);
	// bind in vao the vPosition attribute to position data in vbo
	glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, sizeof(surface_vertex), NULL);
	// set texture coordinate attribute index according to layout specifier in vertex shader
	GLuint vTexCoord = 1;
	// enable in vao the vTexCoord attribute
	glEnableVertexAttribArray(vTexCoord);
	// bind in vao the vTexCoord attribute to tex_coord data in vbo
	glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(surface_vertex),
		(void*)offsetof(surface_vertex, surface_vertex::tex_coord));
	// set normal attribute index according to layout specifier in vertex shader
	GLuint vNormal = 2;
	// enable in vao the vNormal attribute
	glEnableVertexAttribArray(vNormal);
	// bind in vao the vNormal attribute to normal data in vbo
	glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, sizeof(surface_vertex),
		(void*)offsetof(surface_vertex, surface_vertex::normal));
	// check for separate color buffer
	if (color_buffer_id > -1) {
		// allow to set a pointer to a different vertex buffer object
		glBindBuffer(GL_ARRAY_BUFFER, color_buffer_id);
		// set color attribute index according to layout specifier in vertex shader
		GLuint vColor = 3;
		// enable in vao the vColor attribute
		glEnableVertexAttribArray(vColor);
		// bind in vao the vColor attribute to normal data in vbo
		glVertexAttribPointer(vColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, 4, NULL);
	}
	// make vertex array slot empty
	glBindVertexArray(0);

	return vao_id;
}