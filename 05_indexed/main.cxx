#include "../01_context/context.h"
#include "../02_pipeline/pipeline.h"
#include "geometry.h"

#define RESTART_INDEX GLint(-1)

GLuint vertex_shader_id = -1, fragment_shader_id = -1, shader_program_id = -1;
GLint  color_mode_id = -1;

// tesselation parameters
int N = 512, M = 512;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
		return;
	std::cout << "------------------------------------" << std::endl;
	bool processed = check_surface_key(key, mods);
	processed = check_geometry_key(key, mods) || processed;
	if (!processed) {
		switch (key) {
		case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE); break;
		}
	}
	std::cout << "------------------------------------" << std::endl;
}

// prepare context and render objects
void init(GLFWwindow* window, int w, int h)
{
	// define color to clear the color buffer in the glClear() call in draw() function
	glClearColor(0.1f, 0.3f, 1.0f, 1.0f);
	glClearDepth(1.0f);

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	shader_program_id = create_program(
		"flat_shader.glvs", GL_VERTEX_SHADER, &vertex_shader_id,
		"flat_shader.glfs", GL_FRAGMENT_SHADER, &fragment_shader_id, 0);

	init_viewing(window, w, h, shader_program_id);

	glfwSetKeyCallback(window, key_callback);

	color_mode_id = glGetUniformLocation(shader_program_id, "color_mode");

	glPrimitiveRestartIndex(RESTART_INDEX);
	glEnable(GL_PRIMITIVE_RESTART);

	create_geometry(N, M, RESTART_INDEX);
}

// delete render objects
void cleanup()
{
	if (shader_program_id != -1)
		glDeleteProgram(shader_program_id);
	if (vertex_shader_id != -1)
		glDeleteShader(vertex_shader_id);
	if (fragment_shader_id != -1)
		glDeleteShader(fragment_shader_id);
	cleanup_geometry();
}

// draw a triangle with interpolated colors
void display()
{
	// clear the buffers
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	// validity check for shader and vertex array
	if (shader_program_id == -1)
		return;

	// enable shader program
	glUseProgram(shader_program_id);

	glUniform1i(color_mode_id, color_mode);

	draw_geometry(shader_program_id);

	glUseProgram(0);
}

// main function initializes glfw, creates window, initializes glew and app before main loop is started
int main(int argc, char *argv[])
{
	return context_setup_and_main_loop(640, 480, "cg1_indexed_sample");
}
