#pragma once

#include <stdio.h>
#include <GL/glew.h>
#include <string>

#ifdef CG1_SAMPLE_BASEPATH
	#define PREPEND_BASEPATH(path) (std::string(CG1_SAMPLE_BASEPATH"/")+path)
#else
	#define PREPEND_BASEPATH(path) (path)
#endif

GLuint create_texture_y(const std::vector<uint8_t>& data, int w, int h, bool generate_mipmap = true)
{
	GLuint tex_id = -1;
	glGenTextures(1, &tex_id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, &data[0]);
	if (generate_mipmap)
		glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	return tex_id;
}

GLuint create_texture_rgb(const std::vector<uint8_t>& data, int w, int h, bool generate_mipmap = true)
{
	GLuint tex_id = -1;
	glGenTextures(1, &tex_id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);
	if (generate_mipmap)
		glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	return tex_id;
}

GLuint read_texture_rgb_raw(const std::string& file_name, int w, int h, bool generate_mipmap = true)
{
	GLuint tex_id = -1;
	FILE* fp = fopen(PREPEND_BASEPATH(file_name).c_str(), "rb");
	if (fp) {
		std::vector<uint8_t> data(3 * w * h);
		if (fread(&data[0], w * h, 3, fp) == 3)
			tex_id = create_texture_rgb(data, w, h, generate_mipmap);
	}
	return tex_id;
}

GLuint read_texture_y_raw(const std::string& file_name, int w, int h, bool generate_mipmap = true)
{
	GLuint tex_id = -1;
	FILE* fp = fopen(PREPEND_BASEPATH(file_name).c_str(), "rb");
	if (fp) {
		std::vector<uint8_t> data(w * h);
		if (fread(&data[0], w * h, 1, fp) == 1)
			tex_id = create_texture_y(data, w, h, generate_mipmap);
	}
	return tex_id;
}
