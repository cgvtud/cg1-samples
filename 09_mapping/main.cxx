#define _USE_MATH_DEFINES
#include <cmath>
#include "../01_context/context.h"
#include "../02_pipeline/pipeline.h"
#include "../03_depth_buffer/random_scene.h"
#include "../04_viewing/viewing.h"
#include "../06_geometry_shader/normals.h"
#include "../07_instancing/instanced.h"
#include "../08_lighting/lighting.h"
#include "texture.h"

#define RESTART_INDEX GLint(-1)

// texture ids
GLuint diffuse_tex_id = -1;
GLuint normal_tex_id = -1;
GLuint displacement_tex_id = -1;

bool use_normal_mapping = true;
bool normalize_tangents = true;
bool use_diffuse_mapping = true;
bool use_displacement_mapping = true;

float displacement_scale = 1.0f;
int steps = 25;

// smooth shader
GLuint vertex_shader_id = -1, fragment_shader_id = -1, lighting_shader_id = -1, shader_program_id = -1;
GLint  color_mode_id = -1;

// tesselation shader
bool wireframe = false;
bool use_tesselation = false;
float tessellation_level = 8;
GLuint tesselation_vertex_shader_id = -1, tesselation_control_shader_id = -1, 
	   tesselation_evaluation_shader_id = -1, tesselation_fragment_shader_id = -1, 
	   tesselation_lighting_shader_id = -1, tesselation_shader_program_id = -1;

// normal shader
GLuint normal_vs_id = -1, normal_gs_id = -1, normal_fs_id = -1, normal_sp_id = -1;
GLuint normal_length_id = -1;

// secondary representation for height field
GLuint hfield_base_vbo_id = -1;
GLuint hfield_base_vao_id = -1;
GLuint hfield_diffuse_tex_id = -1;
GLuint hfield_normal_tex_id = -1;
GLuint hfield_displacement_tex_id = -1;

// tesselation parameters
int N = 256, M = 256;

// instancing parameters
GLint shader_instancing_scale_id = -1;
GLint normal_instancing_scale_id = -1;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE)
		return;

	std::cout << "------------------------------------" << std::endl;
	const char* color_mode_names[] = { "color", "position", "tex coords", "normal", "lighting", "displacement" };
	std::cout << "<C|P|N|T|L|X> color_mode = " << color_mode_names[color_mode] << std::endl;

	bool processed = check_surface_key(key, mods, false);
	processed = check_instanced_geometry_key(key, mods, false) || processed;

	// give extended help for instanced geometry
	const char* object_mode_names[] = { "height field", "mesh", "texture height field" };
	std::cout << "<H|M|J> object_mode   = " << object_mode_names[object_mode] << std::endl;
	std::cout << "<S|W> nr_instances  = " << nr_instances << std::endl;

	processed = check_normal_key(key, mods) || processed;
	processed = check_lighting_key(key, mods) || processed;
	if (!processed) {
		switch (key) {
		case GLFW_KEY_L: color_mode = 4; processed = true; break;
		case GLFW_KEY_X: color_mode = 5; processed = true; break;
		case GLFW_KEY_J: object_mode = 2; processed = true; break;
		case GLFW_KEY_8: use_diffuse_mapping = !use_diffuse_mapping; processed = true; break;
		case GLFW_KEY_9: use_normal_mapping = !use_normal_mapping; processed = true; break;
		case GLFW_KEY_B: normalize_tangents = !normalize_tangents; processed = true; break;
		case GLFW_KEY_0: use_tesselation = !use_tesselation; processed = true; break;
		case GLFW_KEY_A: if (tessellation_level > 1) --tessellation_level; processed = true; break;
		case GLFW_KEY_D: ++tessellation_level; processed = true; break;
		case GLFW_KEY_W: wireframe = !wireframe; processed = true; break;
		case GLFW_KEY_F6: if (steps > 0) --steps; processed = true; break;
		case GLFW_KEY_F7: ++steps; processed = true; break;
		case GLFW_KEY_F8: displacement_scale *= 0.95f; processed = true; break;
		case GLFW_KEY_F9: displacement_scale /= 0.95f; processed = true; break;
		case GLFW_KEY_F10: use_displacement_mapping = !use_displacement_mapping; processed = true; break;
		case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE); processed = true; break;
		}
	}
	std::cout << "<8> diffuse mapping = " << (use_diffuse_mapping ? "on" : "off") << std::endl;
	std::cout << "<9> normal mapping  = " << (use_normal_mapping ? "on" : "off") << std::endl;
	std::cout << "<B> normalize tangents = " << (normalize_tangents ? "on" : "off") << std::endl;
	std::cout << "<F10>   displacement mapping = " << (use_displacement_mapping ? "on" : "off") << std::endl;
	std::cout << "<F6/F7> displacement steps   = " << steps << std::endl;
	std::cout << "<F8/F9> displacement scale   = " << (object_mode == 1 ? 0.01f*displacement_scale : displacement_scale) << std::endl;
	std::cout << "<0> tesselation mapping = " << (use_tesselation ? "on" : "off") << std::endl;
	std::cout << "<A|D> tesselation level = " << tessellation_level << std::endl;
	std::cout << "<W> wireframe = " << (wireframe ? "on" : "off") << std::endl;
	std::cout << "------------------------------------" << std::endl;
}

// prepare context and render objects
void init(GLFWwindow* window, int w, int h)
{
	color_mode = 4;
	show_normals = false;
	// define color to clear the color buffer in the glClear() call in draw() function
	glClearColor(0.1f, 0.3f, 1.0f, 1.0f);
	glClearDepth(1.0f);

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	shader_program_id = create_program(
		"../07_instancing/flat_shader.glvs", GL_VERTEX_SHADER, &vertex_shader_id,
		"../08_lighting/lighting.glsl", GL_FRAGMENT_SHADER, &lighting_shader_id,
		"smooth_shader.glfs", GL_FRAGMENT_SHADER, &fragment_shader_id, 0);

	normal_sp_id = create_program(
		"../07_instancing/normal_shader.glvs", GL_VERTEX_SHADER, &normal_vs_id,
		"../06_geometry_shader/normal_shader.glgs", GL_GEOMETRY_SHADER, &normal_gs_id,
		"../06_geometry_shader/normal_shader.glfs", GL_FRAGMENT_SHADER, &normal_fs_id, 0);

	tesselation_shader_program_id = create_program(
		"../07_instancing/flat_shader.glvs", GL_VERTEX_SHADER,          &tesselation_vertex_shader_id,
						   "tesselate.gltc", GL_TESS_CONTROL_SHADER,    &tesselation_control_shader_id,
						   "tesselate.glte", GL_TESS_EVALUATION_SHADER, &tesselation_evaluation_shader_id,
			 "../08_lighting/lighting.glsl", GL_FRAGMENT_SHADER,		&tesselation_lighting_shader_id,
						   "tesselate.glfs", GL_FRAGMENT_SHADER,		&tesselation_fragment_shader_id, 0);

	init_viewing(window, w, h, shader_program_id, normal_sp_id, tesselation_shader_program_id);

	glfwSetKeyCallback(window, key_callback);

	color_mode_id = glGetUniformLocation(shader_program_id, "color_mode");
	normal_length_id = glGetUniformLocation(normal_sp_id, "normal_length");
	shader_instancing_scale_id = glGetUniformLocation(shader_program_id, "instance_scale");
	normal_instancing_scale_id = glGetUniformLocation(normal_sp_id, "instance_scale");

	glPrimitiveRestartIndex(RESTART_INDEX);
	glEnable(GL_PRIMITIVE_RESTART);

	const mesh_info* mi_ptr = create_instanced_geometry(N, M, RESTART_INDEX);
	if (!mi_ptr->diffuse_tex_file_name.empty())
		diffuse_tex_id = (mi_ptr->diffuse_channels == 3 ? read_texture_rgb_raw : read_texture_y_raw)(
			mi_ptr->diffuse_tex_file_name, mi_ptr->diffuse_W, mi_ptr->diffuse_H, true);
	if (!mi_ptr->normal_tex_file_name.empty())
		normal_tex_id = (mi_ptr->normal_channels == 3 ? read_texture_rgb_raw : read_texture_y_raw)(
			mi_ptr->normal_tex_file_name, mi_ptr->normal_W, mi_ptr->normal_H, true);
	if (!mi_ptr->displacement_tex_file_name.empty())
		displacement_tex_id = (mi_ptr->displacement_channels == 3 ? read_texture_rgb_raw : read_texture_y_raw)(
			mi_ptr->displacement_tex_file_name, mi_ptr->displacement_W, mi_ptr->displacement_H, true);
	//****************************************************
	//* create texture based height field representation *
	//****************************************************
	// create vertex buffer with 6 vertices for triangle rendering of the height field corners
	std::vector<surface_vertex> V;
	float u0 = 1.0f / (2 * N);
	float u1 = 1.0f - u0;
	float v0 = 1.0f / (2 * M);
	float v1 = 1.0f - v0;
	V.push_back(surface_vertex{ glm::vec3(-1,0,-1), glm::vec2(u0,v0), glm::vec3(0,1,0) });
	V.push_back(surface_vertex{ glm::vec3( 1,0,-1), glm::vec2(u1,v0), glm::vec3(0,1,0) });
	V.push_back(surface_vertex{ glm::vec3(-1,0, 1), glm::vec2(u0,v1), glm::vec3(0,1,0) });
	V.push_back(surface_vertex{ glm::vec3(-1,0, 1), glm::vec2(u0,v1), glm::vec3(0,1,0) });
	V.push_back(surface_vertex{ glm::vec3( 1,0,-1), glm::vec2(u1,v0), glm::vec3(0,1,0) });
	V.push_back(surface_vertex{ glm::vec3( 1,0, 1), glm::vec2(u1,v1), glm::vec3(0,1,0) });
	hfield_base_vbo_id = create_buffer(V, true);
	// create vertex array object without instance offset attribute
	hfield_base_vao_id = create_surface_vertex_array(hfield_base_vbo_id, -1);
	// set instance offset attribute of newly created vertex array object
	GLint vInstanceOffset = 4;
	glm::vec3 zero_offset(2.0f, 0.0f, 0.0f);
	glBindVertexArray(hfield_base_vao_id);
	glBindBuffer(GL_ARRAY_BUFFER, instance_offset_vbo_id);
	glEnableVertexAttribArray(vInstanceOffset);
	glVertexAttribPointer(vInstanceOffset, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3),
		(void*)(max_nr_instances * sizeof(glm::vec3)));
	glVertexAttribDivisor(vInstanceOffset, 1);
	glVertexAttrib3fv(vInstanceOffset, glm::value_ptr(zero_offset));
	glBindVertexArray(0);
	// temporarily construct the geometry of the height field as data source for textures
	V.clear();
	std::vector<glm::u8vec4> C;
	construct_height_field_vertices(height_field, N, M, V, &C);
	// as the textures of the height field are of odd dimension and the pixels have 3 bytes
	// the default unpack alignment of 4 has to be overwritten to 1
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	// extract information for textures
	int i, j;
	std::vector<uint8_t> diffuse_data, normal_data, displacement_data;
	for (j=0; j<M; ++j) {
		for (i = 0; i < N; ++i) {
			float height            = height_field[j*N + i];
			const surface_vertex& v = V[j*(N + 1) + i];
			const glm::u8vec4& c    = C[j*(N + 1) + i];
			diffuse_data.push_back(c[0]);
			diffuse_data.push_back(c[1]);
			diffuse_data.push_back(c[2]);
			// first convert normal into tangent space, where it is not normalized
			glm::vec3 nm(0.5f*v.normal[0], 0.5f*v.normal[2], v.normal[1]);
			// next encode into rgb values
			nm = 255 * 0.5f*(nm + 1.0f);
			normal_data.push_back((uint8_t)nm[0]);
			normal_data.push_back((uint8_t)nm[1]);
			normal_data.push_back((uint8_t)nm[2]);
			// scale height by a factor of 0.5 when encoding the displacements
			displacement_data.push_back((uint8_t)(255 * (0.5f*height + 0.5f)));
		}
	}
	// create textures
	hfield_diffuse_tex_id = create_texture_rgb(diffuse_data, N, M);
	hfield_normal_tex_id = create_texture_rgb(normal_data, N, M);
	hfield_displacement_tex_id = create_texture_y(displacement_data, N, M);
}

// delete render objects
void cleanup()
{
	if (shader_program_id != -1)
		glDeleteProgram(shader_program_id);
	if (vertex_shader_id != -1)
		glDeleteShader(vertex_shader_id);
	if (lighting_shader_id != -1)
		glDeleteShader(lighting_shader_id);
	if (fragment_shader_id != -1)
		glDeleteShader(fragment_shader_id);

	if (normal_sp_id != -1)
		glDeleteProgram(normal_sp_id);
	if (normal_vs_id != -1)
		glDeleteShader(normal_vs_id);
	if (normal_gs_id != -1)
		glDeleteShader(normal_gs_id);
	if (normal_fs_id != -1)
		glDeleteShader(normal_fs_id);

	if (tesselation_shader_program_id != -1)
		glDeleteProgram(tesselation_shader_program_id);
	if (tesselation_vertex_shader_id != -1)
		glDeleteShader(tesselation_vertex_shader_id);
	if (tesselation_control_shader_id != -1)
		glDeleteShader(tesselation_control_shader_id);
	if (tesselation_evaluation_shader_id != -1)
		glDeleteShader(tesselation_evaluation_shader_id);
	if (tesselation_lighting_shader_id != -1)
		glDeleteShader(tesselation_lighting_shader_id);
	if (tesselation_fragment_shader_id != -1)
		glDeleteShader(tesselation_fragment_shader_id);

	if (diffuse_tex_id != -1)
		glDeleteTextures(1, &diffuse_tex_id);
	if (normal_tex_id != -1)
		glDeleteTextures(1, &normal_tex_id);
	if (displacement_tex_id != -1)
		glDeleteTextures(1, &displacement_tex_id);

	cleanup_instanced_geometry();
}

void display()
{
	// clear the buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, wireframe ? GL_LINE : GL_FILL);

	// validity check for shader and vertex array
	if (shader_program_id == -1)
		return;

	// enable shader program
	GLuint prog_id = (use_tesselation&&(object_mode>0)) ? tesselation_shader_program_id : shader_program_id;
	glUseProgram(prog_id);
	glUniform1i(color_mode_id, color_mode);
	set_lighting_parameters(prog_id);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, object_mode == 1 ? diffuse_tex_id : hfield_diffuse_tex_id);
	set_shader_uniform(prog_id, "diffuse_texture", 0);
	set_shader_uniform(prog_id, "use_diffuse_mapping", use_diffuse_mapping && object_mode != 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, object_mode == 1 ? normal_tex_id : hfield_normal_tex_id);
	set_shader_uniform(prog_id, "normal_texture", 1);
	set_shader_uniform(prog_id, "use_normal_mapping", use_normal_mapping && object_mode != 0);
	set_shader_uniform(prog_id, "normalize_tangents", normalize_tangents && object_mode != 0);
	// set displacement texture not in case of mesh when mesh does not have displacement texture
	if (!(object_mode == 1 && displacement_tex_id == -1)) {
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, object_mode == 1 ? displacement_tex_id : hfield_displacement_tex_id);
		set_shader_uniform(prog_id, "displacement_texture", 2);
		set_shader_uniform(prog_id, "use_displacement_mapping", use_displacement_mapping && object_mode != 0);
	}
	else
		set_shader_uniform(prog_id, "use_displacement_mapping", false);

	if (use_tesselation)
		set_shader_uniform(prog_id, "tessellation_level", tessellation_level);
	else
		set_shader_uniform(prog_id, "steps", steps);
	set_shader_uniform(prog_id, "displacement_scale", object_mode == 1 ? 0.01f*displacement_scale : displacement_scale);

	// in case of texture based height field representation
	if (object_mode == 2) {
		// draw texture it with a triangle strip directly from vertex arrays
		set_viewing_parameters(prog_id);
		glBindVertexArray(hfield_base_vao_id);
		if (nr_instances == 1)
			glDrawArrays(use_tesselation ? GL_PATCHES : GL_TRIANGLES, 0, 6);
		else
			glDrawArraysInstanced(use_tesselation ? GL_PATCHES : GL_TRIANGLES, 0, 6, nr_instances);
	}
	else
		// otherwise use default implementation
		draw_geometry(prog_id, false, nr_instances, use_tesselation&&(object_mode>0));

	// do not display normals for texture based height field representation
	if (object_mode != 2)
		if (show_normals && normal_sp_id != -1) {
			glUseProgram(normal_sp_id);
			// normal length is defined in object coordinates and needs to be scaled
			// for mesh as mesh is rendered with model view that uniformly scales and 
			// translates mesh such that maximum extent maps to [-1, 1]
			glUniform1f(normal_length_id,
				object_mode == 0 ? normal_length :
				normal_length * mesh_scale);
			draw_geometry(normal_sp_id, true, nr_instances);
		}
	glUseProgram(0);
}

// main function initializes glfw, creates window, initializes glew and app before main loop is started
int main(int argc, char *argv[])
{
	return context_setup_and_main_loop(1024, 768, "cg1_mapping_sample");
}
