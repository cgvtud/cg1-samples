#version 430

layout(triangles, fractional_odd_spacing) in;

in vec3 patch_position[];
in vec3 patch_position_eye[];
in vec3 patch_normal_eye[];
in vec2 patch_tex_coord[];
in vec4 patch_color[];

patch in vec3 patch_tangent_eye;
patch in vec3 patch_bi_tangent_eye;

out vec3 position;
out vec3 position_eye;
out vec3 normal_eye;
out vec2 tex_coord;
out vec4 color;

out vec3 tangent_eye;
out vec3 bi_tangent_eye;

uniform mat4 projectionMatrix;

uniform sampler2D displacement_texture;
uniform float displacement_scale;
uniform bool use_displacement_mapping = false;

void main()
{
	vec3 barycentric = gl_TessCoord;

	//do simple barycentric interpolation
	position     = barycentric.x * patch_position[0] + barycentric.y * patch_position[1] + barycentric.z * patch_position[2];
	position_eye = barycentric.x * patch_position_eye[0] + barycentric.y * patch_position_eye[1] + barycentric.z * patch_position_eye[2];
	normal_eye   = normalize(barycentric.x * patch_normal_eye[0]   + barycentric.y * patch_normal_eye[1]   + barycentric.z * patch_normal_eye[2]);
	tex_coord    = barycentric.x * patch_tex_coord[0] + barycentric.y * patch_tex_coord[1] + barycentric.z * patch_tex_coord[2];
	color        = barycentric.x * patch_color[0] + barycentric.y * patch_color[1] + barycentric.z * patch_color[2];

	tangent_eye = patch_tangent_eye;
	bi_tangent_eye = patch_bi_tangent_eye;

	if(use_displacement_mapping)
		position_eye += normal_eye * displacement_scale * (2.0 * texture(displacement_texture, tex_coord).r - 1.0);

	gl_Position = projectionMatrix * vec4(position_eye, 1.0);
}