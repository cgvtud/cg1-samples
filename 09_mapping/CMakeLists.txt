
# Add package build target
set(SAMPLE_PROJ_NAME "cg1_09_mapping_sample")
addSample(${SAMPLE_PROJ_NAME})

# Collect package sources
addSourcesToSample(${SAMPLE_PROJ_NAME} FILES main.cxx)
addHeadersToSample(${SAMPLE_PROJ_NAME} FILES texture.h)
addShadersToSample(${SAMPLE_PROJ_NAME} FILES tesselate.gltc tesselate.glte tesselate.glfs smooth_shader.glfs)

# Set up dependencies
target_link_libraries(${SAMPLE_PROJ_NAME} libglew_shared glfw glm)

# Debug config for Visual Studio
set_property(TARGET ${SAMPLE_PROJ_NAME} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
