#pragma once

#include <glm/glm.hpp>
#include <random>

// create an interleaved vertex buffer with position and color with randomly distributed triangles
void create_random_triangle_scene(std::vector<glm::vec3>& vertex_data, int nr_triangles, float edge_length_scale)
{
	// declare vertex data with one vertex per line containing 4d position and 4d color values
	std::default_random_engine r;
	std::uniform_real_distribution<float> d(-1.0f, 1.0f);
	for (int i = 0; i < nr_triangles; ++i) {
		glm::vec3 anchor(d(r), d(r), d(r));
		glm::vec3 edge_vector_1(d(r), d(r), d(r));
		glm::vec3 edge_vector_2(d(r), d(r), d(r));
		glm::vec3 e1 = edge_length_scale * edge_vector_1;
		glm::vec3 e2 = edge_length_scale * edge_vector_2;
		glm::vec3 p1 = anchor + e1;
		glm::vec3 p2 = anchor + e2;
		glm::vec3 color = 0.5f * (anchor + glm::vec3(1.0f));
		vertex_data.push_back(anchor);
		vertex_data.push_back(color);
		vertex_data.push_back(anchor + edge_length_scale * edge_vector_1);
		vertex_data.push_back(color);
		vertex_data.push_back(anchor + edge_length_scale * edge_vector_2);
		vertex_data.push_back(color);
	}
}